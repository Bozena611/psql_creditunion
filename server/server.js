require('dotenv').config();
const express = require("express");
const app = express();
const morgan = require("morgan");
const router = express.Router();
const cors = require("cors");
const db = require('./db');
const port = process.env.PORT || 3005;



//middleware
app.use(cors());
app.use(express.json()); //req.body

app.use(morgan('tiny'));

// ====ROUTES=====
const GenderRoutes = require('./routes/GenderRoutes');
const PersonTitleRoutes = require('./routes/PersonTitleRoutes');
const ProfTitleRoutes = require('./routes/ProfTitleRoutes');
const EducationRoutes = require('./routes/EducationRoutes');
const CountryRoutes = require('./routes/CountryRoutes');
const NationalityRoutes = require('./routes/NationalityRoutes');
const PersonRoutes = require('./routes/PersonRoutes');


//===== USE ROUTES ====
app.use('/credit-union', router);
router.use('/', GenderRoutes);
router.use('/', PersonTitleRoutes);
router.use('/', ProfTitleRoutes);
router.use('/', EducationRoutes);
router.use('/', CountryRoutes);
router.use('/', NationalityRoutes);
router.use('/', PersonRoutes);





app.listen (port, () => {
	console.log (`server is running on port ${port}`)
});