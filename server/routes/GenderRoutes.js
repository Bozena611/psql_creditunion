const express = require('express');
const router = express.Router();
const controller = require('../controllers/GenderControllers');



//get all genders
router.get ('/gender', controller.getGenders);

// get one gender
router.get("/gender/:gender_id", controller.getOneGender);

// create new gender
router.post("/gender", controller.createGender);

// update a gender
router.put("/gender/:gender_id", controller.updateGender);

// delete one gender
router.delete("/gender/:gender_id", controller.removeGender);

module.exports = router;
