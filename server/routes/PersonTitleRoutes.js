const express = require('express');
const router = express.Router();
const controller = require('../controllers/PersonTitleControllers');



//get all person-titles
router.get ('/person-title', controller.getTitles);

// get one
router.get("/person-title/:person_title_id", controller.getOneTitle);

// create new person-title
router.post("/person-title", controller.createPersonTitle);

// update a person-title
router.put("/person-title/:person_title_id", controller.updatePersonTitle);

// delete one
router.delete("/person-title/:person_title_id", controller.removePersonTitle);

module.exports = router;