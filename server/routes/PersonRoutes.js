const express = require('express');
const router = express.Router();
const controller = require('../controllers/PersonControllers');



//get all persons
router.get ('/persons', controller.getPersons);

// get one person
router.get("/persons/:person_id", controller.getOnePerson);

// create new person
router.post("/persons", controller.createPerson);

// update a person
router.put("/persons/:person_id", controller.updatePerson);

// delete one person
router.delete("/persons/:person_id", controller.removePerson);

module.exports = router;