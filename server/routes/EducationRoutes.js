const express = require('express');
const router = express.Router();
const controller = require('../controllers/EducationControllers');



//get all education
router.get ('/education', controller.getEducations);

// get one education
router.get("/education/:education_id", controller.getOneEducation);

// create new education
router.post("/education", controller.createEducation);

// update a education
router.put("/education/:education_id", controller.updateEducation);

// delete one education
router.delete("/education/:education_id", controller.removeEducation);

module.exports = router;