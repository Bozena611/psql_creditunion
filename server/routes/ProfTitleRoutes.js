const express = require('express');
const router = express.Router();
const controller = require('../controllers/ProfTitleControllers');



//get all prof-titles
router.get ("/prof-title", controller.getProfTitles);

// get one prof-title
router.get("/prof-title/:professional_title_id", controller.getOneProfTitle);

// create new prof-title
router.post("/prof-title", controller.createProfTitle);

// update a prof-title
router.put("/prof-title/:professional_title_id", controller.updateProfTitle);

// delete one prof-title
router.delete("/prof-title/:professional_title_id", controller.removeProfTitle);

module.exports = router;