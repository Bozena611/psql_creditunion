const express = require('express');
const router = express.Router();
const controller = require('../controllers/NationalityControllers');



//get all nationalities
router.get ('/nationality', controller.getNationalities);

// get one nationality
router.get("/nationality/:nat_id", controller.getOneNationality);

// create new nationality
router.post("/country/:country_id/nationality", controller.createNationality);

// update a nationality
router.put("/country/:country_id/nationality/:nat_id", controller.updateNationality);

// delete one nationality ???? which route to use ????

//router.delete("/country/:id/nationality/:nat_id", controller.removeNationality);
router.delete("/nationality/:nat_id", controller.removeNationality);

module.exports = router;