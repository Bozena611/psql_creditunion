const express = require('express');
const router = express.Router();
const controller = require('../controllers/CountryControllers');



//get all countries
router.get ('/country', controller.getCountries);

// get one country
router.get("/country/:country_id", controller.getOneCountry);

// create new country
router.post("/country", controller.createCountry);

// update a country
router.put("/country/:country_id", controller.updateCountry);

// delete one country
router.delete("/country/:country_id", controller.removeCountry);

module.exports = router;