const db = require('../db/index');
//app.use(express.json());


class NationalityControllers {

	//GET all nationalities

	async getNationalities (req, res) {
	  try {
	    const allNationalities = await db.query("SELECT * FROM nationality");
	    res.json(allNationalities.rows);
	  } catch (err) {
	    console.error(err.message);
	  }
	};

	//get a nationality

	async getOneNationality (req, res) {
	  try {
	    const { nat_id } = req.params;
	    const oneNationality = await db.query("SELECT * FROM nationality WHERE nat_id = $1", [
	      nat_id
	    ]);

	    res.json(oneNationality.rows[0]);
	  } catch (err) {
	    console.error(err.message);
	  	}
	};


	//create a NEW nationality

 async createNationality (req, res) {
	
  try {
  	let { country_id } = req.params;
    let { name, description } = req.body;
    const newNationality = await db.query(
      "INSERT INTO nationality (name, description, country_id) VALUES ($1, $2, $3) RETURNING *",
      [name, description, country_id]
    );
    //console.log(req.body);s
    console.log(newNationality.rows);
    res.json(newNationality.rows[0]);
  } catch (err) {
    console.error(err.message);
  }
};

//update a nationality

	async updateNationality (req, res) {
	  try {
	    const { nat_id } = req.params;
	    const { country_id } = req.params;
	    const { name, description } = req.body;
	    const changeNationality = await db.query(
	      "UPDATE nationality SET name = $1, description = $2, country_id = $3 WHERE nat_id = $4",
	      [name, description, country_id, nat_id]
	    );

	    res.json(`Nationality ${nat_id} was updated!`);
	  } catch (err) {
	    console.error(err.message);
	  	}
	};


//delete a nationality

async removeNationality (req, res) {
  try {
    const { nat_id } = req.params;
    const deleteNationality = await db.query("DELETE FROM nationality WHERE nat_id = $1", [
      nat_id
    ]);
    res.json("Deleted id: " + nat_id);
  } catch (err) {
    console.log(err.message);
  }
};

}


module.exports = new NationalityControllers();