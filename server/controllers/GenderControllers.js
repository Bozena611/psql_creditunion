const db = require('../db/index');
//app.use(express.json());


class GenderControllers {

	//GET all genders

	async getGenders (req, res) {
	  try {
	    const allGenders = await db.query("SELECT * FROM gender");
	    res.json(allGenders.rows);
	  } catch (err) {
	    console.error(err.message);
	  }
	};

	//get a gender

	async getOneGender (req, res) {
	  try {
	    const { gender_id } = req.params;
	    const oneGender = await db.query("SELECT * FROM gender WHERE gender_id = $1", [
	      gender_id
	    ]);
	    console.log(oneGender.rows[0]);
	    res.json(oneGender.rows[0]);
	  } catch (err) {
	    console.error(err.message);
	  	}
	};


	//create a NEW GENDER

 async createGender (req, res) {
	
  try {
    let { mark, description } = req.body;
    const newGender = await db.query(
      "INSERT INTO gender (mark, description) VALUES ($1, $2) RETURNING *",
      [mark, description]
    );
    //console.log(req.body);
    console.log(newGender.rows);
    res.json(newGender.rows[0]);
  } catch (err) {
    console.error(err.message);
  }
};

//update a gender

	async updateGender (req, res) {
	  try {
	    const { gender_id } = req.params;
	    const { mark, description } = req.body;
	    const updatePTitle = await db.query(
	      "UPDATE gender SET mark = $1, description = $2 WHERE gender_id = $3",
	      [mark, description, gender_id]
	    );

	    res.json(`Gender ${gender_id} was updated!`);
	  } catch (err) {
	    console.error(err.message);
	  	}
	};


//delete a gender

async removeGender (req, res) {
  try {
    const { gender_id } = req.params;
    const deleteGender = await db.query("DELETE FROM gender WHERE gender_id = $1", [
      gender_id
    ]);
    res.json("Deleted id: " + gender_id);
  } catch (err) {
    console.log(err.message);
  }
};


}


module.exports = new GenderControllers();