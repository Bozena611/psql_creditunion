const db = require('../db/index');
//app.use(express.json());


class EducationControllers {

	//GET all education

	async getEducations (req, res) {
	  try {
	    const allEducation = await db.query("SELECT * FROM education");
	    res.json(allEducation.rows);
	  } catch (err) {
	    console.error(err.message);
	  }
	};

	//get an education

	async getOneEducation (req, res) {
	  try {
	    const { education_id } = req.params;
	    const oneEducation = await db.query("SELECT * FROM education WHERE education_id = $1", [
	      education_id
	    ]);

	    res.json(oneEducation.rows[0]);
	  } catch (err) {
	    console.error(err.message);
	  	}
	};


	//create a NEW education

 async createEducation (req, res) {
	
  try {
    let { name, description } = req.body;
    const newEducation = await db.query(
      "INSERT INTO education (name, description) VALUES ($1, $2) RETURNING *",
      [name, description]
    );
    //console.log(req.body);
    console.log(newEducation.rows);
    res.json(newEducation.rows[0]);
  } catch (err) {
    console.error(err.message);
  }
};

//update a education

	async updateEducation (req, res) {
	  try {
	    const { education_id } = req.params;
	    const { name, description } = req.body;
	    const updateEdu = await db.query(
	      "UPDATE education SET name = $1, description = $2 WHERE education_id = $3",
	      [name, description, education_id]
	    );

	    res.json(`Education ${education_id} was updated!`);
	  } catch (err) {
	    console.error(err.message);
	  	}
	};


//delete a education

async removeEducation (req, res) {
  try {
    const { education_id } = req.params;
    const deleteEducation = await db.query("DELETE FROM education WHERE education_id = $1", [
      education_id
    ]);
    res.json("Deleted education_id: " + education_id);
  } catch (err) {
    console.log(err.message);
  }
};


}


module.exports = new EducationControllers();