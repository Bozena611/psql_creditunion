const db = require('../db/index');
//app.use(express.json());


class CountryControllers {

	//GET all countries

	async getCountries (req, res) {
	  try {
	    const allCountries = await db.query("SELECT * FROM country");
	    res.json(allCountries.rows);
	  } catch (err) {
	    console.error(err.message);
	  }
	};

	//get a country

	async getOneCountry (req, res) {
	  try {
	    const { country_id } = req.params;
	    const oneCountry = await db.query("SELECT * FROM country WHERE country_id = $1", [
	      country_id
	    ]);

	    res.json(oneCountry.rows[0]);
	  } catch (err) {
	    console.error(err.message);
	  	}
	};


	//create a NEW country

 async createCountry (req, res) {
	
  try {
    let { name, telcode } = req.body;
    const newCountry = await db.query(
      "INSERT INTO country (name, telcode) VALUES ($1, $2) RETURNING *",
      [name, telcode]
    );
    //console.log(req.body);
    console.log(newCountry.rows);
    res.json(newCountry.rows[0]);
  } catch (err) {
    console.error(err.message);
  }
};

//update a country

	async updateCountry (req, res) {
	  try {
	    const { country_id } = req.params;
	    const { name, telcode } = req.body;
	    const changeCountry = await db.query(
	      "UPDATE country SET name = $1, telcode = $2 WHERE country_id = $3",
	      [name, telcode, country_id]
	    );
	    console.log('body :', req.body);
	    console.log('params :', req.params);
	    res.json(`Country ${country_id} was updated!`);
	  } catch (err) {
	    console.error(err.message);
	  	}
	};


//delete a country

async removeCountry (req, res) {
  try {
    const { country_id } = req.params;
    const deleteCountry = await db.query("DELETE FROM country WHERE country_id = $1", [
      country_id
    ]);
    res.json("Deleted id: " + country_id);
  } catch (err) {
    console.log(err.message);
  }
};

}


module.exports = new CountryControllers();