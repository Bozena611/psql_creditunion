const db = require('../db/index');

class PersonTitleControllers {

	//GET all titles

	async getTitles (req, res) {
	  try {
	    const allTitles = await db.query("SELECT * FROM person_title");
	    res.json(allTitles.rows);
	  } catch (err) {
	    console.error(err.message);
	  }
	};

	//get one title

	async getOneTitle (req, res) {
	  try {
	    const { person_title_id } = req.params;
	    const oneTitle = await db.query("SELECT * FROM person_title WHERE person_title_id = $1", [
	      person_title_id
	    ]);

	    res.json(oneTitle.rows[0]);
	  } catch (err) {
	    console.error(err.message);
	  	}
	};


	//create a NEW persontitle

	async createPersonTitle (req, res) {
	  try {
	    let { name, description } = req.body;
	    const newTitle = await db.query(
	      "INSERT INTO person_title (name, description) VALUES ($1, $2) RETURNING *",
	      [name, description]
	    );
	    //console.log(req.body);
	    console.log(newTitle.rows);
	    res.json(newTitle.rows[0]);
	  } catch (err) {
	    console.error(err.message);
	  	}
	};


//update a title

	async updatePersonTitle (req, res) {
	  try {
	    const { person_title_id } = req.params;
	    const { name, description } = req.body;
	    const updatePTitle = await db.query(
	      "UPDATE person_title SET name = $1, description = $2 WHERE person_title_id = $3",
	      [name, description, person_title_id]
	    );

	    res.json("Person_Title was updated!");
	  } catch (err) {
	    console.error(err.message);
	  	}
	};


//delete a person-title

	async removePersonTitle (req, res) {
	  try {
	    const { person_title_id } = req.params;
	    const deletePersonTitle = await db.query("DELETE FROM person_title WHERE person_title_id = $1", [
	      person_title_id
	    ]);
	    res.json("Deleted id: " + person_title_id);
	  } catch (err) {
	    console.log(err.message);
	  }
	};


}


module.exports = new PersonTitleControllers();