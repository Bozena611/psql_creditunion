const db = require('../db/index');
//app.use(express.json());


class PersonControllers {

	//GET all persons

	async getPersons (req, res) {
	  try {
	    const allPersons = await db.query("SELECT * FROM person");
	    res.json(allPersons.rows);
	  } catch (err) {
	    console.error(err.message);
	  }
	};

	//get a person

	async getOnePerson (req, res) {
	  try {
	    const { person_id } = req.params;
	    const onePerson = await db.query("SELECT * FROM person WHERE person_id = $1", [
	      person_id
	    ]);
	    if(onePerson.rows[0] === undefined) {
	    	throw new Error (`Error: id does not exist`)
	    } else {

	    res.json(onePerson.rows[0]);
	  }
	  } catch (err) {
	    console.error(err.message);
	  	}
	};


	//create a NEW person

	async createPerson (req, res) {
	
	  try {
	    let { lastname, firstname, birthdate, remark, gender_id, person_title_id, professional_title_id, education_id, country_id, nation_id} = req.body;
	    const newPerson = await db.query(
	      "INSERT INTO person (lastname, firstname, birthdate, remark, gender_id, person_title_id, professional_title_id, education_id, country_id, nation_id) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING *",
	      [lastname, firstname, birthdate, remark, gender_id, person_title_id, professional_title_id, education_id, country_id, nation_id]
	    );
	    //console.log(req.body);s
	    console.log(newPerson.rows);
	    res.json(newPerson.rows[0]);
	  } catch (err) {
	    console.error(err.message);
	  }
	};

//update a person

	async updatePerson (req, res) {
	  try {
	    const { person_id } = req.params;
	    const { lastname, firstname, birthdate, remark, gender_id, person_title_id, professional_title_id, education_id, country_id, nation_id} = req.body;
	    const changePerson = await db.query(
	      "UPDATE person SET lastname = $1, firstname = $2, birthdate = $3, remark = $4, gender_id = $5, person_title_id = $6, professional_title_id = $7, education_id = $8, country_id = $9, nation_id = $10  WHERE person_id = $11",
	      [lastname, firstname, birthdate, remark, gender_id, person_title_id, professional_title_id, education_id, country_id, nation_id, person_id]
	    );

	    res.json(`Person ${person_id} was updated!`);
	  } catch (err) {
	    console.error(err.message);
	  	}
	};


//delete a person

	async removePerson (req, res) {
	  try {
	    const { person_id } = req.params;
	    const deleteNationality = await db.query("DELETE FROM person WHERE person_id = $1", [
	      person_id
	    ]);
	    res.json("Deleted id: " + person_id);
	  } catch (err) {
	    console.log(err.message);
	  }
	};

}


module.exports = new PersonControllers();