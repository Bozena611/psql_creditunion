const db = require('../db/index');


class ProfTitleControllers {

	//GET all profTitles

	async getProfTitles (req, res) {
	  try {
	    const allProfTitles = await db.query("SELECT * FROM professional_title");
	    res.json(allProfTitles.rows);
	  } catch (err) {
	    console.error(err.message);
	  }
	};

	//get one title

	async getOneProfTitle (req, res) {
	  try {
	    const { professional_title_id } = req.params;
	    const oneProfTitle = await db.query("SELECT * FROM professional_title WHERE professional_title_id = $1", [
	      professional_title_id
	    ]);

	    res.json(oneProfTitle.rows[0]);
	  } catch (err) {
	    console.error(err.message);
	  	}
	};


	//create a NEW proftitle

	async createProfTitle (req, res) {
	  try {
	    let { name, description } = req.body;
	    const newProfTitle = await db.query(
	      "INSERT INTO professional_title (name, description) VALUES ($1, $2) RETURNING *",
	      [name, description]
	    );
	    //console.log(req.body);
	    console.log(newProfTitle.rows);
	    res.json(newProfTitle.rows[0]);
	  } catch (err) {
	    console.error(err.message);
	  	}
	};


//update a proftitle

	async updateProfTitle (req, res) {
	  try {
	    const { professional_title_id } = req.params;
	    const { name, description } = req.body;
	    const updateProfTitle = await db.query(
	      "UPDATE professional_title SET name = $1, description = $2 WHERE professional_title_id = $3",
	      [name, description, professional_title_id]
	    );

	    res.json("Professional_Title was updated!");
	  } catch (err) {
	    console.error(err.message);
	  	}
	};


//delete a prof-title

	async removeProfTitle (req, res) {
	  try {
	    const { professional_title_id } = req.params;
	    const deleteProfTitle = await db.query("DELETE FROM professional_title WHERE professional_title_id = $1", [
	      professional_title_id
	    ]);
	    res.json("Deleted id: " + professional_title_id);
	  } catch (err) {
	    console.log(err.message);
	  }
	};


}



module.exports = new ProfTitleControllers();
